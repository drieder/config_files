# Post Install Actions

Things to do on a fresh install.  These things should be automated soon(tm)

## Install the following:

- alacritty
- tmux
- neovim
- vimplug 
- brave browser
- pcmanfm
- git

## git & environment tasks: 

- ```git clone --bare https://gitlab.com/drieder/config_files.git $HOME/.cfg```
- ```alias config='/usr/bin/git --git-dir=$HOME/.cfg/ --work-tree=$HOME'```
add this alias until .bashrc and/or .zshrc are pulled down:
- ```config checkout```
- Create new ssh key and, install it with gitlab web ui. **Always use a password 
  with your key.**  If password-less interaction is desired, use ssh-agent, either
  when starting X, tmux, or the shell itself, then authenticate with ssh-add
    ```ssh-keygen -t ed25519``` 
- Change upstream git repository auth from https to ssh
    config remote -v (to see what you are using)
    config remote set-url origin git@gitlab.com:drieder@config_files.git

